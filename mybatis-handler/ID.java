package com.makshi.framework.core.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.makshi.framework.core.json.IDJsonDeserializer;

@JsonSerialize(using=ToStringSerializer.class)
@JsonDeserialize(using=IDJsonDeserializer.class)
public class ID {
	private static final String ID_PERF = "L";
	private final long id;

	private ID(String id) {
		if(id == null || id.length()==0) {
			throw new IllegalArgumentException("id must not be null.");
		}
		this.id = Long.valueOf(id.substring(1));
	}
	
	private ID(long id) {
		this.id = id;
	}
	
	public static ID valueOf(String id) {
		if(id == null || id.length()==0) {
			return null;
		}
		if(!id.startsWith(ID_PERF)) {
			throw new IllegalArgumentException("id must start with \""+ID_PERF+"\"");
		}
		return new ID(id);
	}
	
	public static ID valueOf(long id) {
		return new ID(id);
	}
	
	public String getVal() {
		return toString();
	}
	
	public long getLongVal() {
		return id;
	}
	
	public String toString() {	
		return ID_PERF+id;
	}
}
