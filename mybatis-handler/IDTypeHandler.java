package com.makshi.framework.web.db.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import com.makshi.framework.core.entity.ID;

@MappedJdbcTypes(value = JdbcType.BIGINT, includeNullJdbcType=true)
public class IDTypeHandler extends BaseTypeHandler<ID>{

	@Override
	public void setNonNullParameter(PreparedStatement ps, int i, ID parameter, JdbcType jdbcType) throws SQLException {
		ps.setLong(i, parameter==null?0:parameter.getLongVal());
	}

	@Override
	public ID getNullableResult(ResultSet rs, String columnName) throws SQLException {
		return ID.valueOf(rs.getLong(columnName));
	}

	@Override
	public ID getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
		return ID.valueOf(rs.getLong(columnIndex));
	}

	@Override
	public ID getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
		return ID.valueOf(cs.getLong(columnIndex));
	}

}
